window.onload = () => {

  const emailField = document.querySelector('#email-field');
  const pwdField = document.querySelector('#password-field');
  const loginBtn = document.querySelector('#submit-btn');

  loginBtn.addEventListener('click', event => {
    fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: emailField.value,
        password: pwdField.value
      })
    })
    .then(res => res.json())
    .then(body => {
      const { auth, token, nickname } = body;
      if (auth) {
        localStorage.setItem('jwt', token);
        localStorage.setItem('nickname', nickname);
        location.replace('/game');
      } else {
        alert('auth failed!');
      }
    })
    .catch(err => console.error(err));
  })
}