
window.onload = async () => {
  const jwt = localStorage.getItem('jwt');

  if (!jwt) {
    location.replace('/login');
  } else {
    let progress;
    let curIndex = 0;
    let needNewText = true;
    let lvlContentResponse, lvlContent, lvlChars, lvlCharsLength;
    const textEl = document.querySelector('#text');
    const progressContainer = document.querySelector('#progress-container');
    const nickname = localStorage.getItem('nickname');

    const fetchText = async () => {
      lvlContentResponse = await fetch('/level');
      lvlContent = await lvlContentResponse.json();
      lvlChars = lvlContent.level.split('');
      lvlCharsLength = lvlChars.length;
      needNewText = false;

      curIndex = 0;

      Array.from(document.getElementsByTagName('progress')).map(el => {
        el.value = 0;
      });

      textEl.innerHTML = null;
      lvlChars.map((char, index) => {
        const el = document.createElement('div');
        el.classList.add('char');
        el.id = index;
        el.innerHTML = char === ' ' ? '&nbsp;' : char;
        textEl.append(el);
      });
    }
    
    const socket = io.connect('http://localhost:5000');

    socket.on('connect', async () => {
      await fetchText();
      createProgressRaw(nickname, lvlCharsLength, progressContainer);
      progress = document.getElementById(nickname);
      socket.emit('room', { nickname });
    })

    socket.on('disconnect', () => {
      socket.emit('close', { nickname });
    })

    socket.on('updateNicknames', ({ roomNicknames }) => {
      roomNicknames.map(nickname => {
        const el = document.getElementById(nickname);
        if (!el) {
          createProgressRaw(nickname, lvlCharsLength, progressContainer);
        }
      });
    })

    socket.on('message', ({ nickname, value }) => {
      document.getElementById(nickname).value = value;
    });

    socket.on('timer', async ({ status, duration }) => {
      const timerContainer = document.getElementsByClassName('timer-container')[0];
      const timeContainer = document.getElementById('time');
      const textContainer = document.getElementById('text');
      const mainContainer = document.querySelector('main');
      const textAfterTimerContainer = document.getElementById('text-after-timer');
      const textBeforeTimerContainer = document.getElementById('text-before-timer');
      const minutes = parseInt(duration / 60, 10);
      const seconds = parseInt(duration % 60, 10);

      if (status === 'wait') {
        timeContainer.innerHTML = `${minutes}m ${seconds}s`;
        needNewText = true;
        return;
      }

      if (status === '20seconds') {
        textAfterTimerContainer.style.display = 'none';
        textBeforeTimerContainer.style.display = 'block';
        mainContainer.style.display = 'block';
        timerContainer.style.transform = 'translate(50%, 50%)';
        timerContainer.style.bottom = '50%';
        timerContainer.style.right = '50%';
        textEl.style.display = 'none';
        timeContainer.innerHTML = `${minutes}m ${seconds}s`;
        needNewText = true;
        return;
      }

      if (needNewText) {
        await fetchText();
      }

      textEl.style.display = 'flex';
      textBeforeTimerContainer.style.display = 'none';
      textAfterTimerContainer.style.display = 'block';
      mainContainer.style.display = 'block';
      timerContainer.style.transform = 'unset';
      timerContainer.style.bottom = '30px';
      timerContainer.style.right = '30px';
      timeContainer.innerHTML = `${minutes}m ${seconds}s`;
    })

    document.addEventListener('keydown', event => {
      const char = event.key;
      if (lvlChars[curIndex] === char) {
        document.getElementById(curIndex).classList.add('complete');
        socket.emit('reload', { finish: ++curIndex === lvlCharsLength, nickname, value: curIndex });
        progress.value = curIndex;
      }
    });
  }
}

const createProgressRaw = (nickname, max, container) => {
  const progress = document.createElement('progress');
  progress.id = nickname;
  progress.max = max;

  const nicknameEl = document.createElement('span');
  nicknameEl.innerHTML = nickname;

  container.append(nicknameEl, progress);
}