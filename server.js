const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const fs = require('fs');

require('./passport.config');
let level;

const generateLvl = () => {
  const countOfLevels = fs.readdirSync(path.join(__dirname, 'texts')).length;
  const rndLvlNumber = Math.floor(Math.random() * countOfLevels) + 1;
  const lvlFilename = `text${rndLvlNumber}.txt`;
  level = fs.readFileSync(path.join(__dirname, 'texts', lvlFilename), 'utf8');
}

server.listen(5000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/game', (req, res) => {
  res.sendFile(path.join(__dirname, 'game.html'));
});

app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, 'login.html'));
});

// protect
app.get('/level', /* passport.authenticate('jwt'),*/(req, res) => {
  if (level) {
    res.status(200).json({ level });
  } else {
    res.status(400).json({ err: 'error' });
  }
});

app.post('/login', (req, res) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.email === userFromReq.email);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token, nickname: userInDB.nickname });
  } else {
    res.status(401).json({ auth: false });
  }
});

const WAITING_DURATION = 5;
const RACE_DURATION = 10;

const serverRoom = 'room1';
let roomNicknames = new Set();
let waitingPlayers = new Set();
let isRace = false;
let duration;

const timer = () => {
  generateLvl();

  duration = WAITING_DURATION;

  setInterval(() => {
    if (--duration === 0) {
      isRace = !isRace;
      if (isRace) {
        generateLvl();
        duration = RACE_DURATION;
        waitingPlayers.clear();
        io.emit('updateNicknames', { roomNicknames: Array.from(roomNicknames) })
      } else {
        duration = WAITING_DURATION;
      }
    } else {
      io.in(serverRoom).emit('timer', { status: isRace ? 'game' : '20seconds', duration });
    }
  }, 1000)
}

timer();

io.on('connection', socket => {
  console.log('connected ');

  socket.on('room', ({ nickname }) => {
    console.log('nickname ', nickname);
    let playerDuration = !isRace ? duration + RACE_DURATION + WAITING_DURATION : duration + WAITING_DURATION;
    roomNicknames.add(nickname);

    // wait for joining to room
    const interval = setInterval(() => {
      if (--playerDuration === 0) {
        clearInterval(interval);
        console.log('socket join');
        socket.join(serverRoom);
      } else {
        socket.emit('timer', { status: 'wait', duration: playerDuration });
      }
    }, 1000);

    waitingPlayers.add(nickname);
  })

  socket.in(serverRoom).on('reload', ({ finish, nickname, value }) => {
    console.log('reload');
    if (finish) {
      io.in(serverRoom).emit('finish');
    } else {
      io.to(serverRoom).emit('message', { nickname, value });
    }
  })

  socket.on('disconnect', () => {
    console.log('disconnected');

    /////
  })
});